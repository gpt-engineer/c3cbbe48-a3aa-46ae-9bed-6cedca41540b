document.getElementById('task-form').addEventListener('submit', function(event) {
    event.preventDefault();
    var taskInput = document.getElementById('task-input');
    var taskList = document.getElementById('task-list');
    if (taskInput.value.trim() !== '') {
        var newTask = document.createElement('li');
        newTask.innerHTML = `
            <input type="checkbox">
            <span class="mx-2">${taskInput.value}</span>
            <button class="delete-button text-red-500"><i class="fas fa-trash-alt"></i></button>
        `;
        newTask.classList.add('flex', 'items-center', 'justify-between', 'p-2', 'border', 'rounded', 'bg-white');
        taskList.appendChild(newTask);
        taskInput.value = '';
    }
});

document.getElementById('task-list').addEventListener('click', function(event) {
    if (event.target.classList.contains('delete-button')) {
        event.target.parentElement.remove();
    }
});
